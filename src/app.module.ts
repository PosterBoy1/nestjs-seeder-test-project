import {Module} from '@nestjs/common';
import {SequelizeModule} from '@nestjs/sequelize';
import {SeederModule} from 'nestjs-sequelize-seeder/dist';
import {UsersModule} from './users/users.module';

@Module({
    imports: [
        SequelizeModule.forRootAsync({
            useFactory: () => {
                return {
                    dialect: 'mysql',
                    host: 'localhost',
                    port: 3306,
                    username: 'root',
                    password: 'root',
                    database: 'music',
                    logging: true, // put this one true if you want to see what sql commands sequelize is executing in the logs
                    autoLoadModels: true,
                    synchronize: true,

                    sync: {
                        force: true,
                    },
                };
            },
        }),
        SeederModule.forRoot({
            isGlobal: true,
            logging: true,
            connection: 'music',
            enableAutoId: true,
            autoIdFieldName: 'id',
            foreignDelay: 1000,
        }),
        UsersModule,
    ],
})
export class AppModule {
}
