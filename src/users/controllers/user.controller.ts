import {Controller, Get, Param} from '@nestjs/common';
import {UserService} from '../services/user.service';
import {User} from '../models/User';

@Controller('user')
export class UserController {

    constructor(private userService: UserService) {
    }

    @Get(':username')
    async getUserByUsername(@Param('username') username: string): Promise<User> {
        return this.userService.findOne(username);
    }

}
