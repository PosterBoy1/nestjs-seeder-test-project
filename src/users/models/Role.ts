import {AutoIncrement, BelongsToMany, Column, Model, PrimaryKey, Table} from 'sequelize-typescript';
import {UserRole} from './UserRole';
import {User} from './User';

@Table
export class Role extends Model<Role> {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @Column
    role: string;

    @BelongsToMany(
        () => User,
        () => UserRole,
        // 'userId',
        // 'roleId',
    )
    users: UserRole[];
}
