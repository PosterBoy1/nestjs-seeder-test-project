import {Column, ForeignKey, Model, PrimaryKey, Table} from 'sequelize-typescript';
import {User} from './User';
import {Role} from './Role';

@Table
export class UserRole extends Model<UserRole> {
    @ForeignKey(() => User)
    @PrimaryKey
    @Column
    userId: number;
    // @BelongsTo(() => User)
    // user: User;

    @ForeignKey(() => Role)
    @PrimaryKey
    @Column
    roleId: number;
    // @BelongsTo(() => Role)
    // role: Role;
}


