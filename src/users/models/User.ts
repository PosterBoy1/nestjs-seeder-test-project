import {AutoIncrement, BelongsToMany, Column, IsEmail, Model, PrimaryKey, Table, Unique} from 'sequelize-typescript';
import {UserRole} from './UserRole';
import {Role} from './Role';

@Table
export class User extends Model<User> {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @IsEmail
    @Unique
    @Column
    email: string;

    @Unique
    @Column
    username: string;

    @Column
    password: string;

    @Column
    salt: string;

    @BelongsToMany(
        () => Role,
        () => UserRole,
        // 'roleId',
        // 'userId',
    )
    roles: UserRole[];
}
