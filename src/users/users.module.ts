import {Module} from '@nestjs/common';
import {UserService} from './services/user.service';
import {SequelizeModule} from '@nestjs/sequelize';
import {SeederModule} from 'nestjs-sequelize-seeder/dist';
import {User} from './models/User';
import {SeedUsers} from './seeders/SeedUsers';
import {UserController} from './controllers/user.controller';
import {UserRole} from './models/UserRole';
import {Role} from './models/Role';
import {SeedRoles} from './seeders/SeedRoles';
import {SeedUserRoles} from './seeders/SeedUserRoles';
import {RoleService} from './services/role.service';

@Module({
    imports: [
        SequelizeModule.forFeature([
            User,
            Role,
            UserRole,
        ]),
        SeederModule.forFeature([
            SeedUsers,
            SeedRoles,
            SeedUserRoles,
        ]),
    ],
    providers: [UserService, RoleService],
    exports: [UserService, RoleService],
    controllers: [UserController],
})
export class UsersModule {
}
