import {Injectable} from '@nestjs/common';
import {Role} from '../models/Role';

@Injectable()
export class RoleService {

    async findOne(id: number): Promise<Role> {
        return Role.findOne({where: {id}});
    }
}
