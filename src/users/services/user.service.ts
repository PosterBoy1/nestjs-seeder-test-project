import {Injectable} from '@nestjs/common';
import {User} from '../models/User';
import {UserRole} from '../models/UserRole';

@Injectable()
export class UserService {

    async findOne(username: string): Promise<User> {
        return User.findOne(
            {
                where: {username},
                include: [UserRole],
            },
        ).catch(
            error => {
                throw error;
            },
        );
    }
}
