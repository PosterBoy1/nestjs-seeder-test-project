import {UserRole} from '../models/UserRole';
import {OnSeederInit, Seeder} from 'nestjs-sequelize-seeder/dist';

@Seeder({
    model: UserRole,
    containsForeignKeys: true,
})
export class SeedUserRoles implements OnSeederInit {
    run() {
        return [
            // {id: 1, userId: 1, roleId: 1},
            // {id: 2, userId: 1, roleId: 3},
            // {id: 3, userId: 2, roleId: 1},
            // {id: 4, userId: 3, roleId: 2},
            {userId: 1, roleId: 1},
            {userId: 1, roleId: 3},
            {userId: 2, roleId: 1},
            {userId: 3, roleId: 2},
        ] as UserRole[];
    }
}

