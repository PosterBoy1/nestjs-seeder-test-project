import {OnSeederInit, Seeder} from 'nestjs-sequelize-seeder/dist';
import {Role} from '../models/Role';

@Seeder({
    model: Role,
})
export class SeedRoles implements OnSeederInit {
    run() {
        return [
            // {id: 1, role: 'admin'},
            // {id: 2, role: 'user'},
            {role: 'admin'},
            {role: 'user'},

        ] as Role[];
    }
}
