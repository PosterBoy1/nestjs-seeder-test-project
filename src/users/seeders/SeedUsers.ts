import {OnSeederInit, Seeder} from 'nestjs-sequelize-seeder/dist';
import {User} from '../models/User';
import {genSaltSync, hashSync} from 'bcrypt';

@Seeder({
    model: User,
})
export class SeedUsers implements OnSeederInit {
    run() {
        return [
            {
                username: 'john',
                email: 'john@gmail.com',
                password: 'password1',
            },
            {
                username: 'chris',
                email: 'chris@gmail.com',
                password: 'password2',
            },
            {
                username: 'maria',
                email: 'maria@gmail.com',
                password: 'password3',
            },
        ] as User[];
    }

    everyone(user: User) {
        if (user.password) {
            const salt = genSaltSync(10);
            user.password = hashSync(user.password, salt);
            user.salt = salt;
        }

        user.createdAt = new Date().toISOString();
        user.updatedAt = new Date().toISOString();

        return user;
    }
}
